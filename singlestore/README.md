# Singlestore

[SingleStore](https://www.singlestore.com/solutions/#use-cases) is a scalable SQL database that ingests data continuously to perform operational analytics. Ingests millions of events per second with ACID transactions while simultaneously analyzing billions of rows of data in relational SQL, JSON, geospatial, and full-text search formats.

### How to run on local machine
Ensure you have docker-compose [installed](https://docs.docker.com/compose/install/).
```
cd singlestore
docker-compose up -d
```